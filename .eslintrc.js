module.exports = {
    "root": true,
    "extends": "airbnb/base",
    "env": {
        "jasmine": true,
        "protractor": true
    },
    "globals": {
        "angular": true,
        "module": true,
        "inject": true,
        "_": true
    },
    "rules": {
        "wrap-iife":[2,'inside'],
        "comma-dangle": [2, "never"],
        "no-var": 1,
        "prefer-const": 0,
        "strict": [0, "global"],
        "indent": [2, 4],
        "func-names": [0],
        "new-cap": [2, {"capIsNewExceptions": ["express.Router"]}],
        "space-before-function-paren": [2, { "anonymous": "always", "named": "never" }],
        'no-underscore-dangle': [0, { 'allowAfterThis': false }],
        'eol-last': 0,
        'object-curly-spacing': [0, 'always'],
        'arrow-body-style': [0, 'as-needed'],
        'max-len': [0, 150, 4],
        "no-use-before-define": 0,
        "no-shadow": 1,
        "eqeqeq": [2, "smart"]
    }
};
